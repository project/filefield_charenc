<?php
/**
 * @file
 * Filefield Character Encoding
 * Adds the ability to convert character encoding on upload of text files to CCK filefields.
 *
 * Copyright 2010 by Matthew Davidson (http://drupal.org/user/52996)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Get from|to encoding for a file
 */
function filefield_charenc_get_encoding($fid, $op = 'from') {
  $result = db_query("SELECT * FROM {filefield_charenc} WHERE fid = %d", $fid);
  $stored = db_fetch_array($result);
  
  if ($stored) {
    return $stored[$op];
  }
  
  return NULL;
}

/**
 * Checks to see if to|from encodings have changed.
 *
 * @param $file A file object.
 * @return FALSE if an encoding has changed, or TRUE if not.
 */
function filefield_charenc_check_encoding(&$file) {
  $result = db_query("SELECT * FROM {filefield_charenc} WHERE fid = %d", $file->fid);
  $stored = db_fetch_array($result);
  
  if ($stored && ($file->filefield_charenc['from'] == $stored['from']) && ($file->filefield_charenc['to'] == $stored['to'])) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}


/**
 * Does what it says on the tin.
 *
 * @param $file A file object.
 * @return TRUE if file has been successfully converted.
 */
function filefield_charenc_convert_encoding(&$file) {
  $data = file_get_contents($file->filefield_charenc['source']);
  $data = mb_convert_encoding($data, $file->filefield_charenc['to'], $file->filefield_charenc['from']);
  $update = array();
  if (file_put_contents($file->filepath, $data)) {
    if (db_fetch_array(db_query("SELECT * FROM {filefield_charenc} WHERE fid = %d", $file->fid))) {
      $result = db_query("UPDATE {filefield_charenc} SET `from` = '%s', `to` = '%s' WHERE fid = %d", $file->filefield_charenc['from'], $file->filefield_charenc['to'], $file->fid);
    }
    else {
      $result = db_query("INSERT INTO {filefield_charenc} (`fid`, `from`, `to`) VALUES (%d, '%s', '%s')", $file->fid, $file->filefield_charenc['from'], $file->filefield_charenc['to']);
    }
    if ($result) {
      return TRUE;
    }
    else {
      drupal_set_message(t('Could not save character encoding information to database. Please contact the site administrator.'), 'error');
      return FALSE;
    }
  }
  else {
    drupal_set_message(t('Could not save converted file data. Please contact the site administrator.'), 'error');
    return FALSE;
  }
}


/**
 * Check that a source file exists. If not, (optionally) create source copy,
 * assuming encoding conversion has just been enabled.
 *
 * @param $file A file object.
 * @param $mode A Boolean value to indicate if the source copy should be created
 *   if it does not exist.
 * @return FALSE when file not found, or TRUE when file exists.
 */
function filefield_charenc_check_source(&$file, $mode = FALSE) {
  $source_dir = dirname($file->filepath) .'/filefield_charenc';
  if (!file_check_directory($source_dir, TRUE)) { // source directory doesn't exist, or can't be created.
    return FALSE;
  }
  
  if (realpath($source_dir .'/'. basename($file->filepath))) {
    $file->filefield_charenc['source'] = $source_dir .'/'. basename($file->filepath);
    return TRUE;
  }
  elseif ($mode) {
    $data = file_get_contents($file->filepath);
    if (file_put_contents($source_dir .'/'. basename($file->filepath), $data)) {
      $file->filefield_charenc['source'] = $source_dir .'/'. basename($file->filepath);
      return TRUE;
    }
  }

  // File doesn't exist and can't be created.
  return FALSE;
}
